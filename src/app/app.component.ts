import { Component, ViewChild } from '@angular/core';
import { Platform, MenuController, NavController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TabsPage } from '../pages/tabs/tabs';
import { SigninPage } from "../pages/signin/signin";
import { AboutPage } from "../pages/about/about";
import { ContactPage } from "../pages/contact/contact";
import { HomePage } from "../pages/home/home";
import { SignupPage } from "../pages/signup/signup";
import { DescriptionPage } from "../pages/description/description";
import { MorePage } from "../pages/more/more";
import { OrderPage } from "../pages/order/order";
import { AppPage } from "../pages/app/app";
import { TermsPage } from "../pages/terms/terms";
import { FindPage } from "../pages/find/find";
import { ProfilePage } from "../pages/profile/profile";
import { PasswordPage } from "../pages/password/password";
import { CartPage } from "../pages/cart/cart";
import { ForgetPage } from "../pages/forget/forget";


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
 

  rootPage: any = SigninPage;
  signin = SigninPage;
  about = AboutPage;
  contact = ContactPage;
  home = HomePage;
  tabs = TabsPage;
  signup = SignupPage;
  description = DescriptionPage;
  more = MorePage;
  order = OrderPage;
  app = AppPage;
  terms = TermsPage;
  find = FindPage;
  profile = ProfilePage;
  password = PasswordPage;
  cart = CartPage;
  forget = ForgetPage;
  
@ViewChild('nav') nav : NavController
  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen,private menuCtrl: MenuController) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      
    });
    
  }

    openPage(page) {
      // Reset the content nav to have just this page
      // we wouldn't want the back button to show in this scenario
      this.nav.setRoot(page.component);
    }
 
     OnLoad(page: any)
    {
      this.nav.setRoot(page);
      this.menuCtrl.close();
    }
  OnOpenMenu()
  {
    this.menuCtrl.open();
  }
}
