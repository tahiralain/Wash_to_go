import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { HttpModule } from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';
import { MyApp } from './app.component';
import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SigninPage } from "../pages/signin/signin";
import { SignupPage } from "../pages/signup/signup";
import { DescriptionPage } from "../pages/description/description";
import { MorePage } from "../pages/more/more";
import { OrderPage } from "../pages/order/order";
import { AppPage } from "../pages/app/app";
import { TermsPage } from "../pages/terms/terms";
import { ProfilePage } from "../pages/profile/profile";
import { FindPage } from "../pages/find/find";
import { NativeStorage } from "@ionic-native/native-storage";
import { PasswordPage } from "../pages/password/password";
import { CartPage } from "../pages/cart/cart";
import { ForgetPage } from "../pages/forget/forget";
import { Camera, CameraOptions } from '@ionic-native/camera';



@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    SigninPage,
    SignupPage,
    DescriptionPage,
    MorePage,
    OrderPage,
    AppPage,
    TermsPage,
    FindPage,
    ProfilePage,
    PasswordPage,
    CartPage,
    ForgetPage,
    TabsPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp, { tabsPlacement: 'bottom', tabsHideOnSubPages: true, })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    SigninPage,
    SignupPage,
    DescriptionPage,
    MorePage,
    OrderPage,
    AppPage,
    CartPage,
    TermsPage,
    FindPage,
    ProfilePage,
    ForgetPage,
    PasswordPage,
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
     HttpModule,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    NativeStorage
  ]
})
export class AppModule { }
