import { Component } from '@angular/core';
import { NavController, MenuController, ModalController, LoadingController } from 'ionic-angular';
import { DescriptionPage } from "../description/description";
import { TermsPage } from "../terms/terms";
import { Http } from '@angular/http';
import { TabsPage } from "../tabs/tabs";

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {
    posts: any;
    data: any;
    categoryID: number = 1;
    public apiUrl = 'http://drycleaningmadeeasy.com/WashToGoApp/API_V1.0/getSubCategories.php?categoryID=' + this.categoryID;
    constructor(public navCtrl: NavController, private menuCtrl: MenuController, private http: Http, private loadingCtrl: LoadingController) {
        console.log(this.apiUrl);
        console.log(this.categoryID);
        this.http.get(this.apiUrl).map(res => res.json()).subscribe(data => {
            console.log(data);
            this.posts = data.data;
            console.log(this.posts);
            // console.log(this.posts);
        }, error => {
            console.log(error); // Error getting the data
        });

    }
    OnOpenMenu() {
        this.menuCtrl.open();
    }
    move() {
        this.navCtrl.push(DescriptionPage);
        // this.navCtrl.push(DescriptionPage,
        //     { {{ post.Sub_Catergory_ID }} });

    }

    myFunction() {
        var x = document.getElementById('myDIV');
        if (x.style.display === 'none') {
            x.style.display = 'block';
        } else {
            x.style.display = 'none';
        }
    }

    showDiv() {
        document.getElementById('welcomeDiv').style.display = "block";
    }
    showhide() {
        var div = document.getElementById("newpost");
        if (div.style.display !== "none") {
            div.style.display = "none";
        }
        else {
            div.style.display = "block";
        }
    }

    opencard(urlText: String, urlText2: String, urlText3: String, urlText4: String) {
        let videoId = { VideoID: urlText };
        this.navCtrl.push(DescriptionPage, { VideoID: urlText, Title_image: urlText2, Cat_des: urlText3, Cat_Name: urlText4 });
        console.log(urlText);
    }

    nav1() { this.categoryID = 1; this.api(); }
    nav2() { this.categoryID = 2; this.api(); }
    nav3() { this.categoryID = 3; this.api(); }
    nav4() { this.categoryID = 4; this.api(); }
    nav5() { this.categoryID = 5; this.api(); }
    nav6() { this.categoryID = 6; this.api(); }
    nav7() { this.categoryID = 7; this.api(); }
    nav8() { this.categoryID = 8; this.api(); }

    api() {

        this.posts;
        this.data;
        this.categoryID;

        let loading = this.loadingCtrl.create({
            content: 'Loading Data....'
        });

        loading.present();

        this.apiUrl = 'http://drycleaningmadeeasy.com/WashToGoApp/API_V1.0/getSubCategories.php?categoryID=' + this.categoryID;

        console.log(this.apiUrl);
        console.log(this.categoryID);
        this.http.get(this.apiUrl).map(res => res.json()).subscribe(data => {
            console.log(data);
            this.posts = data.data;
            loading.dismiss();
        }, error => {
            console.log(error); // Error getting the data
        });
    }
}
