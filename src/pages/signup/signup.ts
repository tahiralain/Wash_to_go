import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, MenuController } from 'ionic-angular';
import { HomePage } from "../home/home";
import { TabsPage } from "../tabs/tabs";
import { Http } from '@angular/http';


@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {

   
   name: String = '';
   email: String = '';
   contact: String = '';
   password: String = '';
   conpassword: String = '';
   delivery_address : String = '';
   private apiUrl = '';
   public address = '';

  constructor(public navCtrl: NavController, private alertCtrl: AlertController, private loadingCtrl: LoadingController, private http: Http,private menu: MenuController) {
  }

//sign-up function
 signUp() {

    if (this.name === '' ||  this.email === '' || this.password === '' || this.contact === '' ||  this.delivery_address === '') {
      let alert = this.alertCtrl.create({
        title: 'Signup Error',
        subTitle: 'All fields are rquired',
        buttons: ['OK']
      });
      alert.present();
      return;
    }


    this.apiUrl = ' http://drycleaningmadeeasy.com/WashToGoApp/API_V1.0/User_Signup.php?name=' + this.name +  '&email=' + this.email + '&contact=' + this.contact + '&password=' + this.password + '&delivery_address=' + this.delivery_address ;
    let loader = this.loadingCtrl.create({
    content: "Signing Up..."
    });
    loader.present();

    var data = {  name: this.name, email: this.email, contact: this.contact, password: this.password, delivery_address: this.delivery_address };
    console.log(data);

    this.http.get(this.apiUrl).map(res => res.json())
      .subscribe(data => {

        console.log(data);
        loader.dismiss();
        this.navCtrl.push(TabsPage);
       
        var status = data.Status;

        if (status === 'User Already Exists') {
          
        let alert = this.alertCtrl.create({
          title: 'Signup Response',
          subTitle:'User Already Exist',
          buttons: ['OK']
        });
        alert.present();
          //this.navCtrl.push(TabsPage);
        }
        else {
         // this.initWallet();
        }

        //redirecting to signin screen after success
      }, error => {
        console.log(error);// Error getting the data

        let alert = this.alertCtrl.create({
          title: 'Signup Response',
          subTitle: data.toString(),
          buttons: ['OK']
        });
        alert.present();

        loader.dismiss();

      });
  }

}
