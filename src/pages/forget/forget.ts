import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, AlertController, LoadingController } from 'ionic-angular';
import { Http } from '@angular/http';
import { SigninPage } from "../signin/signin";

@Component({
  selector: 'page-forget',
  templateUrl: 'forget.html',
})
export class ForgetPage {
  
  email:any;
    private apiUrl = '';
   public address = '';

  constructor(public navCtrl: NavController, private alertCtrl: AlertController, private loadingCtrl: LoadingController, private http: Http,private menu: MenuController) {
  }

   forgot() {

    if (this.email === '') {
      let alert = this.alertCtrl.create({
        title: 'Request Error',
        subTitle: 'Email field should not be empty',
        buttons: ['OK']
      });
      alert.present();
      return;
    }


    this.apiUrl = ' http://drycleaningmadeeasy.com/WashToGoApp/API_V1.0/Forgot_Password.php?email=' + this.email ;
    let loader = this.loadingCtrl.create({
    content: "Sending Request..."
    });
    loader.present();

    var data = {  email: this.email };
    console.log(data);

    this.http.get(this.apiUrl).map(res => res.json())
      .subscribe(data => {

        console.log(data);
        loader.dismiss();
        this.navCtrl.push(SigninPage);
       
        var status = data.Status;

        //redirecting to signin screen after success
      }, error => {
        console.log(error);// Error getting the data

        let alert = this.alertCtrl.create({
          title: 'Request Response',
          subTitle: data.toString(),
          buttons: ['OK']
        });
        alert.present();

        loader.dismiss();

      });
  }

}
