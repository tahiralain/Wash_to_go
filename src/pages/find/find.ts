import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';

@Component({
  selector: 'page-find',
  templateUrl: 'find.html',
})
export class FindPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public menuCtrl : MenuController) {
  }
OnOpenMenu()
  {
    this.menuCtrl.open();
  }
}
