import { Component } from '@angular/core';
import { NavController, NavParams, MenuController } from 'ionic-angular';
import { OrderPage } from "../order/order";
import { AppPage } from "../app/app";
import { FindPage } from "../find/find";
import { PasswordPage } from "../password/password";
import { ProfilePage } from "../profile/profile";
import { TermsPage } from "../terms/terms";
@Component({
  selector: 'page-more',
  templateUrl: 'more.html',
})
export class MorePage {

  constructor(public navCtrl: NavController, public navParams: NavParams,  public menuCtrl : MenuController) {
  }
order(){this.navCtrl.push(OrderPage);}
app(){this.navCtrl.push(AppPage);}
terms(){this.navCtrl.push(TermsPage);}
find(){this.navCtrl.push(FindPage);}
pro(){this.navCtrl.push(ProfilePage);}
pass(){this.navCtrl.push(PasswordPage);}

OnOpenMenu()
  {
    this.menuCtrl.open();
  }
}
