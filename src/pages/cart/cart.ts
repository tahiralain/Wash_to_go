import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';

@Component({
  selector: 'page-cart',
  templateUrl: 'cart.html',
})
export class CartPage {
  Sub_Catergory_ID: any;
  Item_Price: any;
  item_des: any;
  id: any;
  id_name: any;
  id_description: any;
  image_id: any;
  sub_id: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public menuCtrl: MenuController) {

    this.sub_id = this.navParams.get('id_name');
    this.item_des = this.navParams.get('Item_Description');
    this.Item_Price = this.navParams.get('Item_Price');
    this.Sub_Catergory_ID = this.navParams.get('Sub_Catergory_ID');
    console.log('From cart page');
    console.log(this.sub_id);  // yahan get mai panga aaraha hai....
    console.log(this.item_des);
    console.log(this.Item_Price);
    console.log(this.Sub_Catergory_ID);
    //console.log(this.id);
  }

  public event = {
     month:new Date().toISOString(),
     timeStarts: new Date().toTimeString().split(" ")[0]

  }


OnOpenMenu() {
        this.menuCtrl.open();
    }
}
