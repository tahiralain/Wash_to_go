import { Component } from '@angular/core';
import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';
import { DescriptionPage } from "../description/description";
import { MorePage } from "../more/more";
import { CartPage } from "../cart/cart";

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = AboutPage;
  tab3Root = ContactPage;
  tab4Root = CartPage;
  tab5Root = MorePage;
  constructor() {

  }
}
