import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController, ToastController } from 'ionic-angular';
import { TabsPage } from "../tabs/tabs";
import { SignupPage } from "../signup/signup";
import { Http } from "@angular/http";
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';
import { ForgetPage } from "../forget/forget";

@Component({
  selector: 'page-signin',
  templateUrl: 'signin.html',
})
export class SigninPage {

    email: any;
    password: any;
    apiUrl : string;
    loginDetails:any;

   posts:any;
   user: any;
	 userReady: boolean = false;

  constructor(public navCtrl: NavController,  private loadingCtrl: LoadingController, public alertCtrl: AlertController,public http: Http, private storage:Storage, public toastCtrl : ToastController) {
  }
home(){
  this.navCtrl.push(TabsPage);
}
signup(){
  this.navCtrl.push(SignupPage);
}
forgot(){this.navCtrl.push(ForgetPage);}
 
signIn() {

 this.apiUrl = 'http://drycleaningmadeeasy.com/WashToGoApp/API_V1.0/User_Login.php?email='+ this.email +'&password=' + this.password;

     if (this.email === '' || this.password === '') {
       let alert = this.alertCtrl.create({
         title: 'Sign-in Error',
         subTitle: 'Email and Password Required',
        buttons:  ['OK']
       });
       alert.present();
       return;
     }


      let loader = this.loadingCtrl.create({
       content: "Signing In..."
     });
     loader.present();

     console.log(this.apiUrl);
  this.http.get(this.apiUrl).map(res => res.json()).subscribe(data => {
   
        console.log(data);
        this.loginDetails=data;
        console.log("login details",this.loginDetails)
           this.userData();
         var str = data;
        var statusMessage = str.Status;
        if(statusMessage === 'success'){
        this.navCtrl.push(TabsPage)
         }  
       let alert = this.alertCtrl.create({
           title: 'Sign in Response',
           subTitle: 'Welcome to Wash to Go',
           buttons: ['OK']
          
         });
        
         alert.present();
         loader.dismissAll();
     }, error => {
         console.log(error);// Error getting the data

         let alert = this.alertCtrl.create({
           title: 'Sign in Response',
           subTitle: error.toString(),
           buttons: ['OK']
          
         });
        
         alert.present();

         loader.dismissAll();
       });
   
//  this.saveUser();
 }
 // sign-in function end

 userData(){
 this.storage.set('userdata', this.loginDetails);
  }

}
