import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import { CartPage } from "../cart/cart";
import { NativeStorage } from "@ionic-native/native-storage";


@Component({
  selector: 'page-description',
  templateUrl: 'description.html',
})
export class DescriptionPage {
  id_description: any;
  id_name: any;
  posts: any;
  image_id: any;
  sub_id = this.navParams.get('VideoID');  //getting sub-id from previous "homePage"


  public apiUrl = 'http://drycleaningmadeeasy.com/WashToGoApp/API_V1.0/getClothingItems.php?SubCategoryID=' + this.sub_id;
  constructor(public navCtrl: NavController, public navParams: NavParams, private http: Http, private nativeStorage: NativeStorage) {

   // this.sub_id = this.navParams.get('VideoID');
    this.image_id = this.navParams.get('Title_image');
    this.id_description = this.navParams.get('Cat_des');
    this.id_name = this.navParams.get('Cat_Name');

    console.log('VideoID');
     this.http.get(this.apiUrl).map(res => res.json()).subscribe(data => {
            console.log(data);
            this.posts = data.data;
            console.log(this.posts);
            // console.log(this.posts);
        }, error => {
            console.log(error); // Error getting the data
        });
  }

  nextcard(id_name1: String, item_name1: String ,Item_Description1: String, Item_Price1: String, Sub_Catergory_ID1: String) {
        this.navCtrl.push(CartPage, { id_name: id_name1, item_name:item_name1, Item_Description: Item_Description1, Sub_Catergory_ID: Sub_Catergory_ID1, Item_Price: Item_Price1 });
        console.log('From description page');
        console.log(this.id_name);
        console.log(Item_Description1);
        console.log(Item_Price1);
        console.log(Sub_Catergory_ID1);  
      }

//NativeStorage.setItem("reference_to_value",<value>, <success-callback>, <error-callback>);

  

}
